<?php
class buku_controller extends controller
{
    public function index()
    {
        // Load model
        $bukuModel = $this->loadModel('buku_model');
        // Get data from the model
        $bukus = $bukuModel->getAll();
        // Load the view
        $this->loadView('buku_list', ['bukus' => $bukus]);
    }
}
